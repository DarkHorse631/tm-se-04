package ru.grishin.tm.api;

import ru.grishin.tm.entity.Project;

import java.util.Date;

public interface ProjectService {

    void create(String projectName, String description, Date startDate, Date completionDate);

    void remove(String projectId);

    void update(String projectId, String projectName, String description, Date startDate, Date completionDate);

    Project findOne(String projectId);

    void merge(Project project);
}
