package ru.grishin.tm.api;

import ru.grishin.tm.entity.Task;

import java.util.Date;
import java.util.Map;

public interface TaskService {

    void create(String projectId, String taskName, String description, Date startDate, Date completionDate);

    void remove(String id);

    void update(String id, String taskName, String description, Date startDate, Date completionDate);

    Task findOne(String projectId);

    Map<String, Task> findAll();

    void merge(Task task);
}
