package ru.grishin.tm.bootstrap;

import ru.grishin.tm.entity.Project;
import ru.grishin.tm.repository.ProjectRepository;
import ru.grishin.tm.repository.TaskRepository;
import ru.grishin.tm.service.ProjectServiceImpl;
import ru.grishin.tm.service.TaskServiceImpl;
import ru.grishin.tm.view.Command;

import java.util.Date;
import java.util.Map;
import java.util.Scanner;

public class Bootstrap {

    private final ProjectRepository projectRepository = new ProjectRepository();
    private final ProjectServiceImpl projectService = new ProjectServiceImpl(projectRepository);
    private final TaskRepository taskRepository = new TaskRepository();
    private final TaskServiceImpl taskService = new TaskServiceImpl(taskRepository);
    private final Scanner scanner = new Scanner(System.in);

    public void init() {
        System.out.println("[***WELCOME TO TASK MANAGER]");
        do {
            System.out.print("--Input:");
            String input = scanner.nextLine();
            Command command = Command.checkCommand(input);
            if (command.equals(null)) {
                System.out.println("---!!!Wrong command!!!---");
            }
            switch (command) {
                case PROJECT_CREATE:
                    createProject();
                    break;
                case PROJECT_SHOW_ALL:
                    showProjectList();
                    break;
                case PROJECT_DELETE:
                    deleteProject();
                    break;
                case PROJECT_UPDATE:
                    updateProject();
                    break;
                case PROJECT_CLEAR:
                    clearProject();
                    break;
                case PROJECT_MERGE:
                    mergeProject();
                    break;
                case TASK_CREATE:
                    createTask();
                    break;
                case TASK_UPDATE:
                    updateTask();
                    break;
                case TASK_SHOW_ALL:
                    showTaskList();
                    break;
                case TASK_DELETE:
                    deleteTask();
                    break;
                case HELP:
                    showHelpList();
                    break;
                case EXIT:
                    exit();
                    break;
            }
        } while (true);
    }

    private void createProject() {
        System.out.println("--Create project--");
        System.out.print("Enter name: ");
        String name = scanner.nextLine();
        System.out.print("Enter description: ");
        String description = scanner.nextLine();
        projectService.create(name, description, new Date(), new Date());
        System.out.println("[PROJECT CREATED]");
    }

    private void showProjectList() {
        System.out.println("--Show all projects--");
        show(projectService.findAll());
    }

    private void deleteProject() {
        System.out.println("--Remove project--");
        System.out.print("Enter the project id: ");
        String projectId = scanner.nextLine();
        projectService.remove(projectId);
        taskService.deleteByProjectId(projectId);
        System.out.println("[PROJECT DELETED]");
    }

    private void updateProject() {
        System.out.println("--Update project--");
        System.out.print("Enter project id: ");
        String projectId = scanner.nextLine();
        System.out.print("Enter new name: ");
        String name = scanner.nextLine();
        System.out.print("Enter new description: ");
        String description = scanner.nextLine();
        projectService.update(projectId, name, description, new Date(), new Date());
        System.out.println("[PROJECT UPDATED]");
    }

    private void mergeProject() {
        System.out.println("--Merge project--");
        System.out.print("Enter project id: ");
        String projectId = scanner.nextLine();
        System.out.print("Enter project name: ");
        String name = scanner.nextLine();
        System.out.print("Enter project description: ");
        String description = scanner.nextLine();
        projectService.merge(new Project(projectId, name, description, new Date(), new Date()));
        System.out.println("[PROJECT MERGED]");
    }

    private void clearProject() {
        System.out.println("--Clear Project--");
        System.out.print("Enter project id: ");
        String projectId = scanner.nextLine();
        taskService.deleteByProjectId(projectId);
        System.out.println("[PROJECT CLEAR]");
    }

    private void createTask() {
        System.out.println("--Create task into project--");
        System.out.print("Enter project id: ");
        String projectId = scanner.nextLine();
        System.out.print("Enter task name: ");
        String name = scanner.nextLine();
        System.out.print("Enter task description: ");
        String description = scanner.nextLine();
        taskService.create(projectId, name, description, new Date(), new Date());
        System.out.println("[TASK CREATED]");
    }

    private void updateTask() {
        System.out.println("--Update task--");
        System.out.print("Enter task id:");
        String taskId = scanner.nextLine();
        System.out.print("Enter new name:");
        String name = scanner.nextLine();
        System.out.print("Enter new description:");
        String description = scanner.nextLine();
        taskService.update(taskId, name, description, new Date(), new Date());
        System.out.println("[TASK UPDATED]");
    }

    private void deleteTask() {
        System.out.println("--Remove task from project--");
        System.out.print("Enter task id: ");
        String taskId = scanner.nextLine();
        taskService.remove(taskId);
        System.out.println("[TASK DELETED]");

    }

    private void showTaskList() {
        System.out.println("--Task list--");
        show(taskService.findAll());
    }

    private void show(Map<String, ?> map) {
        for (Object o : map.values())
            System.out.println(o);
    }

    private void exit() {
        System.exit(0);
    }

    private void showHelpList() {
        System.out.println("[help]: Show all commands. " +
                "\n [pc]: Create new project." +
                "\n [pu]: Update selected project." +
                "\n [psa]: Show all projects." +
                "\n [pr]: Remove selected project." +
                "\n [pcl]: Remove all tasks from project" +
                "\n" +
                "\n [tc]: Create new task." +
                "\n [tu]: Update selected task." +
                "\n [tsa]: Show all tasks." +
                "\n [tr]: Remove selected task." +
                "\n [tdpi]: Remove all task by project id." +
                "\n " +
                "\n [exit]: Exit from app.");
    }

}
