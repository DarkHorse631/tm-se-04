package ru.grishin.tm.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Task {

    private final String projectId;
    private final String id;
    private String name;
    private String description;
    private Date dateStart;
    private Date dateFinish;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public Task(String projectId, String id, String name, String description, Date dateStart, Date dateFinish) {
        this.projectId = projectId;
        this.id = id;
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Override
    public String toString() {
        return "Project ID = [" + projectId +
                "], Task ID = [" + id +
                "], Task name = [" + name +
                "], Description =[" + description +
                "], Start date = [" + dateFormat.format(getDateStart()) +
                "], Completion date =[" + dateFormat.format(getDateFinish()) + "]";
    }

}
