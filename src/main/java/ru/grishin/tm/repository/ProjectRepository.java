package ru.grishin.tm.repository;

import ru.grishin.tm.entity.Project;

import java.util.*;

public class ProjectRepository {

    private Map<String, Project> projectList = new LinkedHashMap<>();

    public void persist(Project project){
        projectList.put(project.getId(),project);
    }

    public void insert(String id, String name, String description, Date startDate, Date completionDate) {
        projectList.put(id, new Project(id, name, description, startDate, completionDate));
    }

    public void merge(Project project) {
        if (projectList.containsKey(project.getId()))
            update(project.getId(), project.getName(), project.getDescription(), project.getDateStart(), project.getDateFinish());
        else
            insert(project.getId(), project.getName(), project.getDescription(), project.getDateStart(), project.getDateFinish());
    }

    public void mergeAll(Project... projects) {
        for (Project project : projects)
            merge(project);
    }

    public Project findOne(String id) {
        return projectList.get(id);
    }

    public void remove(String id) {
        projectList.remove(id);
    }

    public Map<String, Project> findAll() {
        return projectList;
    }

    public void update(String id, String name, String description, Date startDate, Date completionDate) {
        Project project = projectList.get(id);
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(startDate);
        project.setDateFinish(completionDate);
        projectList.put(id, project);

    }

    public void clearProjectList() {
        projectList.clear();
    }

}
