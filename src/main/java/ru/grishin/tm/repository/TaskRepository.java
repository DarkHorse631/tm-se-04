package ru.grishin.tm.repository;

import ru.grishin.tm.entity.Task;

import java.util.*;

public class TaskRepository {

    private Map<String, Task> taskList = new LinkedHashMap<>();

    public void persist(Task task){
        taskList.put(task.getId(), task);
    }

    public void insert(String projectId, String id, String name, String description, Date dateStart, Date dateFinish) {
        taskList.put(id, new Task(projectId, id, name, description, dateStart, dateFinish));
    }

    public void remove(String id) {
        taskList.remove(id);
    }

    public Task findOne(String id) {
        return taskList.get(id);
    }

    public void deleteByProjectId(String projectId) {
        Iterator<Map.Entry<String, Task>> entryIterator = taskList.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> object = entryIterator.next();
            if (object.getValue().getProjectId().equals(projectId)) entryIterator.remove();
        }
    }

    public Map<String, Task> findAll() {
        return taskList;
    }

    public void merge(Task task) {
        if (taskList.containsKey(task.getId()))
            update(task.getId(), task.getName(), task.getDescription(), task.getDateStart(), task.getDateFinish());
        else
            insert(task.getProjectId(), task.getId(), task.getName(), task.getDescription(), task.getDateStart(), task.getDateFinish());
    }

    public void mergeAll(Task... tasks) {
        for (Task task : tasks) {
            merge(task);
        }
    }

    public void update(String id, String name, String description, Date dateStart, Date dateFinish) {
        Task task = taskList.get(id);
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(dateStart);
        task.setDateFinish(dateFinish);
        taskList.put(task.getId(), task);
    }

    public void removeAll() {
        taskList.clear();
    }

}
