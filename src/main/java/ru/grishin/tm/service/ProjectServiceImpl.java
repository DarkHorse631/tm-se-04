package ru.grishin.tm.service;

import ru.grishin.tm.api.ProjectService;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.repository.ProjectRepository;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(String name, String description, Date dateStart, Date dateFinish) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        projectRepository.persist(new Project(generateRandomBasedUUID(), name, description, dateStart, dateFinish));
    }

    @Override
    public void remove(String id) {
        if (id == null || id.isEmpty()) return;
        projectRepository.remove(id);
    }

    @Override
    public void update(String id, String name, String description, Date dateStart, Date dateFinish) {
        if (id == null || id.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        projectRepository.update(id, name, description, dateStart, dateFinish);
    }

    @Override
    public Project findOne(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOne(id);
    }

    @Override
    public void merge(Project project) {
        if (project.getId() == null || project.getId().isEmpty()) return;
        projectRepository.merge(project);
    }

    public Map<String, Project> findAll() {
        return projectRepository.findAll();
    }

    public String generateRandomBasedUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

}
