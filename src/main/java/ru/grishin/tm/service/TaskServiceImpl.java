package ru.grishin.tm.service;

import ru.grishin.tm.api.TaskService;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.repository.TaskRepository;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(String projectId, String name, String description, Date dateStart, Date dateFinish) {
        if (projectId == null || projectId.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        taskRepository.persist(new Task(projectId, generateRandomBasedUUID(), name, description, dateStart, dateFinish));
    }

    @Override
    public void remove(String id) {
        if (id == null || id.isEmpty()) return;
        taskRepository.remove(id);
    }

    @Override
    public void update(String id, String name, String description, Date dateStart, Date dateFinish) {
        if (id == null || id.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        taskRepository.update(id, name, description, dateStart, dateFinish);
    }

    public void deleteByProjectId(String projectId) {
        taskRepository.deleteByProjectId(projectId);
    }


    @Override
    public Task findOne(String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOne(id);
    }

    @Override
    public Map<String, Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void merge(Task task) {
        if (task.getId() == null || task.getId().isEmpty()) return;
        taskRepository.merge(task);
    }

    public String generateRandomBasedUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
