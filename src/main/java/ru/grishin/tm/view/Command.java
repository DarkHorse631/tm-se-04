package ru.grishin.tm.view;

public enum Command {

    PROJECT_CREATE("pc"),
    PROJECT_READ("pr"),
    PROJECT_DELETE("pd"),
    PROJECT_UPDATE("pu"),
    PROJECT_SHOW_ALL("psa"),
    PROJECT_CLEAR("pcl"),
    PROJECT_MERGE("pm"),
    PROJECT_MERGE_ALL("pma"),


    TASK_CREATE("tc"),
    TASK_DELETE("td"),
    TASK_UPDATE("tu"),
    TASK_MERGE("tm"),
    TASK_DELETE_BY_PROJECT_ID("tdpi"),
    TASK_SHOW_ALL("tsa"),

    HELP("help"),
    EXIT("exit");

    private final String name;

    public static Command checkCommand(String command) {
        Command[] commands = Command.values();
        for (Command o : commands) {
            if (o.getName().equals(command)) return o;
        }
        return null;
    }

    public String getName() {
        return name;
    }

    Command(String name) {
        this.name = name;
    }
}
